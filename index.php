<?php
session_start();
require('dbdata.php');

$q=explode ('/', $_SERVER['REQUEST_URI']);


if ($q[1] == 'user') {
		//user module.
		if ($q[2] == 'signup') {
			signup($q[3],$q[4]);
			} 
		if ($q[2] == 'login') {  
			login($q[3],$q[4]); 
		}		

}
if ($q[1] == 'myaccount') {
		myaccount();
}

if ($q[1] == 'admin') {
		//admin module.
		if ($q[2] == 'login') {  
			login($q[3],$q[4],1);  //login admin
		} else {
			signup($q[2],$q[3],1);  //signup admin
		}
} 
if ($q[1] == 'userlist') {
	userlist();
}


//functions...
function signup ($user,$pass,$admin=0) {
		//simple signup function .
		//password on plain text is not secure!
		global $mysqli;

		if (!isset($user) || !isset($pass)) {
			die('Error. Usage: signup/:username/:password');
		} else {
			$user = $mysqli->real_escape_string($user);
			$pass = $mysqli->real_escape_string($pass);


			$sql = "INSERT INTO users (user, pass,role)
						VALUES ('". $user ."', '". $pass ."', ". intval($admin).")";

			if ($mysqli->query($sql) === TRUE) {

				if ($admin == 1) $adm = '(admin)';

				 echo "User $adm record created successfully";
				} else {
					echo "Error: El usuario no pudo ser creado  <br>\n";
					echo "Error: " . $sql . "\n" . $mysqli->error;
				}

		}
}

function login ($user,$pass,$admin=0) {
		//login function .
		global $mysqli;
		if (!isset($user) || !isset($pass)) {
			die('Error. Usage: login/:username/:password');
		} else {
			$user = $mysqli->real_escape_string($user);
			$pass = $mysqli->real_escape_string($pass);
			$admin = intval($admin);
			$sql = "SELECT user,pass,role from users WHERE user = '".$user."' AND pass = '".$pass."' and role = " . $admin." limit 0,1";
			if ($resultado = $mysqli->query($sql)) {
				if ($resultado->num_rows > 0) {
					//ok.
					//aqui deberia comparar el password, nunca guardar en plain text.
					$_SESSION["user"] = $user;
					$_SESSION['pass'] = $pass;
					$_SESSION['role'] = $admin;
					echo "User $user logged in. <a href='/myaccount'>MyAccount</a>\n";
				} else {
					echo "Error de user o pass (o role) \n";
				}

			} else { // $mysqli->query($sql)
					echo "Error: El usuario no pudo hacer login <br>\n";
					echo "Error: " . $sql .  "<br>\n" . $mysqli->error;				
			}

		$resultado->close();
		}
}

function myaccount(){

	if (isset($_SESSION['user'])) {
		echo '* Your username: ' . $_SESSION['user'] . "<br>\n" ;
		echo '* Your password: ' . $_SESSION['pass'] . "<br>\n" ;
	} else {
		echo "The user is not logged in\n";

	}

}

function userlist(){
	if (isset($_SESSION['user']) && $_SESSION['role'] == 1) { //is admin & logged in
			//show list
		global $mysqli;

			$sql = "SELECT user,pass,role from users";

			if ($resultado = $mysqli->query($sql)) {
					echo "user | pass | (admin) <br>" ; 
			 while ($fila = $resultado->fetch_assoc()) {
			        printf ("%s | %s | (%s) <br>", $fila["user"], $fila["pass"],$fila['role']);
			    }

			 }
			 
	} else {
		echo "Need to be logged in as admin\n";
	}
}



?>
